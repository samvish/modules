<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Sz\CustomPromotion\Block\Adminhtml\SalesRule\Grid;

use Magento\Backend\Block\Widget\Grid\Extended;
/**
 * Customer Segment grid
 *
 * @api
 * @SuppressWarnings(PHPMD.DepthOfInheritance)
 * @since 100.0.2
 */
class Chooser extends \Magento\SalesRule\Block\Adminhtml\Promo\Widget\Chooser
{
    /**
     * Intialize grid
     *
     * @return void
     */
    protected function _construct()
    {
        parent::_construct();
        if ($this->getRequest()->getParam('current_grid_id')) {
            $this->setId($this->getRequest()->getParam('current_grid_id'));
        } else {
            $this->setId('custom_sales_rule_grid_chooser_' . $this->getId());
        }

        $this->setDefaultSort('name');
        $this->setDefaultDir('ASC');
        $this->setUseAjax(true);

        $form = $this->getRequest()->getParam('form');
        if ($form) {
            $this->setRowClickCallback("{$form}.chooserGridRowClick.bind({$form})");
            $this->setCheckboxCheckCallback("{$form}.chooserGridCheckboxCheck.bind({$form})");
            $this->setRowInitCallback("{$form}.chooserGridRowInit.bind({$form})");
        }
        if ($this->getRequest()->getParam('collapse')) {
            $this->setIsCollapsed(true);
        }
    }


    /**
     * Prepare columns for grid
     *
     * @return $this
     */
    protected function _prepareColumns()
    {
        $this->addColumn(
            'rule_id',
            [
                'header_css_class' => 'a-center',
                'type' => 'checkbox',
                'name' => 'Rue Id',
                'values' => $this->_getSelectedParentRules(),
                'align' => 'center',
                'index' => 'rule_id',
                'use_index' => true
            ]
        );
        $this->addColumn(
            'name',
            ['header' => __('Name'), 'align' => 'left', 'width' => '150px', 'index' => 'name']
        );
        $this->addColumn(
            'sort_order',
            ['header' => __('Priority'), 'align' => 'left', 'width' => '150px', 'index' => 'sort_order']
        );
        $this->addColumn(
            'is_active',
            [
                'header' => __('Status'),
                'align' => 'left',
                'width' => '80px',
                'index' => 'is_active',
                'type' => 'options',
                'options' => [1 => 'Active', 0 => 'Inactive']
            ]
        );
        return Extended::_prepareColumns();
    }

    /**
     * Get Selected ids param from request
     *
     * @return array
     */
    protected function _getSelectedParentRules()
    {
        $segments = $this->getRequest()->getPost('selected', []);
        return $segments;
    }

    /**
     * Prepare rules collection
     *
     * @return $this
     */
    protected function _prepareCollection()
    {
        $collection = $this->ruleFactory->create()->getResourceCollection();
        $collection->addFieldToFilter('coupon_type',
            ['neq' => \Magento\SalesRule\Model\Rule::COUPON_TYPE_NO_COUPON]);
        if($currentRuleId = $this->getRequest()->getParam('current_rule_id')) {
            $collection->addFieldToFilter('rule_id',
                ['neq' => $currentRuleId]);
        }
        $this->setCollection($collection);

        $this->_eventManager->dispatch(
            'adminhtml_block_promo_widget_chooser_prepare_collection',
            ['collection' => $collection]
        );

        return Extended::_prepareCollection();
    }

    /**
     * Row click javascript callback getter
     *
     * @return string
     */
    public function getRowClickCallback()
    {
        return $this->_getData('row_click_callback');
    }


    /**
     * Prepare grid URL
     *
     * @return string
     */
    public function getGridUrl()
    {
        return $this->getUrl('custom_sales_rule/promo_quote/choosergrid', ['_current' => true]);
    }
}
