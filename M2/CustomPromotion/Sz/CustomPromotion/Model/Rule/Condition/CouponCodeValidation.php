<?php

namespace Sz\CustomPromotion\Model\Rule\Condition;

use Magento\AdvancedRule\Model\Condition\FilterGroupInterface;
/**
 * Class Customer
 */
class CouponCodeValidation extends \Magento\Rule\Model\Condition\AbstractCondition
{
    /**
     * @var string
     */
    protected $_inputType = 'multiselect';

    /**
     * Adminhtml data
     *
     * @var \Magento\Backend\Helper\Data
     */
    protected $_adminhtmlData;

    protected $_couponCode;

    protected $_requestObject;

    /**
     * @var ConcreteConditionFactory
     */
    protected $concreteConditionFactory;

    /**
     * @var \Magento\AdvancedRule\Model\Condition\FilterableConditionInterface
     */
    protected $concreteCondition = null;

    /**
     * @param \Magento\Rule\Model\Condition\Context $context
     * @param \Magento\Backend\Helper\Data $adminhtmlData
     * @param ConcreteConditionFactory $concreteConditionFactory,
     * @param array $data
     */
    public function __construct(
        \Magento\Rule\Model\Condition\Context $context,
        \Magento\Customer\Model\Session $customerSession,
        \Magento\Backend\Helper\Data $adminhtmlData,
        \Magento\SalesRule\Model\Coupon $coupon,
        \Magento\Framework\App\Request\Http $request,
        array $data = []
    ) {
        $this->_customerSession = $customerSession;
        $this->_adminhtmlData = $adminhtmlData;
        $this->_couponCode = $coupon;
        $this->_requestObject = $request;
        parent::__construct($context, $data);
    }

    /**
     * Default operator input by type map getter
     *
     * @return array
     */
    public function getDefaultOperatorInputByType()
    {
        if (null === $this->_defaultOperatorInputByType) {
            $this->_defaultOperatorInputByType = ['multiselect' => ['==', '()']];
            $this->_arrayInputTypes = ['multiselect'];
        }
        return $this->_defaultOperatorInputByType;
    }

    /**
     * Render chooser trigger
     *
     * @return string
     */
    public function getValueAfterElementHtml()
    {
        return '<a href="javascript:void(0)" class="rule-chooser-trigger"><img src="' .
            $this->_assetRepo->getUrl(
                'images/rule_chooser_trigger.gif'
            ) . '" alt="" class="v-middle rule-chooser-trigger" title="' . __(
                'Open Chooser'
            ) . '" /></a>';
    }

    /**
     * Value element type getter
     *
     * @return string
     */
    public function getValueElementType()
    {
        return 'text';
    }

    /**
     * Chooser URL getter
     *
     * @return string
     */
    public function getValueElementChooserUrl()
    {
        return $this->_adminhtmlData->getUrl(
            'custom_sales_rule/promo_quote/choosergrid',
            [
                'value_element_id' => $this->_valueElement->getId(),
                'form' => $this->getJsFormObject(),
                'current_rule_id' => $this->_requestObject->getParam('id')
            ]
        );
    }

    /**
     * Enable chooser selection button
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getExplicitApply()
    {
        return true;
    }

    /**
     * Render element HTML
     *
     * @return string
     */
    public function asHtml()
    {
        $this->_valueElement = $this->getValueElement();
        return $this->getTypeElementHtml() . __(
                'If Promotion Rule Applied %1 %2',
                $this->getOperatorElementHtml(),
                $this->_valueElement->getHtml()
            ) .
            $this->getRemoveLinkHtml() .
            '<div class="rule-chooser" url="' .
            $this->getValueElementChooserUrl() .
            '"></div>';
    }


    /**
     * @return $this
     */
    public function loadOperatorOptions()
    {
        parent::loadOperatorOptions();
        $this->setOperatorOption(
            [
                '==' => __('matches'),
                '()' => __('is one of'),
            ]
        );
        return $this;
    }

    /**
     * Present selected values as array
     *
     * @return array
     */
    public function getValueParsed()
    {
        $value = $this->getData('value');
        $value = array_map('trim', explode(',', $value));
        return $value;
    }

    /**
     * Validate if qoute customer is assigned to role segments
     *
     * @param   \Magento\Quote\Model\Quote\Address|\Magento\Framework\Model\AbstractModel $object
     * @return  bool
     */
    public function validate(\Magento\Framework\Model\AbstractModel $model)
    {

        if ($model->getQuote()) {
            $appliedCouponCode = $model->getQuote()->getCouponCode();
        }
        if (!isset($appliedCouponCode)) {
            return false;
        }
        if (empty($appliedCouponCode)) {
            return false;
        } else {
            if (!$this->_validateCouponCode($model->getQuote())) {
                return false;
            } else {

                $ruleId =   $this->_couponCode->loadByCode($appliedCouponCode)->getRuleId();
                if ($ruleId) {
                    $ruleId = explode(',', $ruleId);
                    return $this->validateAttribute($ruleId);
                }
            }
        }
        return false;
    }

    protected function _validateCouponCode(\Magento\Quote\Model\Quote $quote)
    {
        $code = $quote->getData('coupon_code');
        $addressHasCoupon = false;
        if (strlen($code)) {
            $addresses = $quote->getAllAddresses();
            if (count($addresses) > 0) {
                foreach ($addresses as $address) {
                    if ($address->hasCouponCode()) {
                        $addressHasCoupon = true;
                    }
                }
            }
        }
        return $addressHasCoupon;
    }

}
