<?php

namespace Sz\CustomPromotion\Controller\Adminhtml\Promo\Quote;

class ChooserGrid extends \Magento\SalesRule\Controller\Adminhtml\Promo\Quote\Index
{
    /**
     * Grid ajax action in chooser mode
     *
     * @return void
     */
    public function execute()
    {
        $this->_view->loadLayout();
        $this->_view->renderLayout();
    }
}
