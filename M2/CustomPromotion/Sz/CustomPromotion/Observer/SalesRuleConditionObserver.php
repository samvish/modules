<?php

namespace Sz\CustomPromotion\Observer;

use Magento\Framework\Event\ObserverInterface;

class SalesRuleConditionObserver implements ObserverInterface
{
    /**
     * Execute observer.
     * @param \Magento\Framework\Event\Observer $observer
     * @return $this
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        $additional = $observer->getEvent()->getAdditional();
        $conditions = (array) $additional->getConditions();

        $conditions = array_merge_recursive($conditions, [
            [
                'label'=> __('Applied Promotion Rule'),
                'value' => \Sz\CustomPromotion\Model\Rule\Condition\CouponCodeValidation::class,
            ],
        ]);

        $additional->setConditions($conditions);
    }

}