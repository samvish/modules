<?php
/**
/**
 * Implements helper functionality.
 *
 * @category   Sz
 * @package    Sz_CustomPromotion
 * @subpackage Config
 * @version    Release: 0.1.0
 * @author     UpBott Team <sam@upbott.com>
 */

class Sz_CustomPromotion_Helper_Data extends Mage_Core_Helper_Abstract
{
    const XML_PATH_GENERAL_ENABLE = 'custompromotion/general/enable';

    /**
     * @param int $storeId
     * @return mixed
     */
    public function isEnable($storeId = 0) {
        return Mage::getStoreConfig(
            self::XML_PATH_GENERAL_ENABLE, $storeId
        );
    }
}