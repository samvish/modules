<?php

class Sz_CustomPromotion_Adminhtml_CustomPromotionController extends Mage_Adminhtml_Controller_Action{

    public function chooserAction()
    {
        $request = $this->getRequest();
        $block = $this->getLayout()->createBlock(
            'custompromotion/adminhtml_chooser_rule', 'promo_widget_chooser_rule',
            array('js_form_object' => $request->getParam('form'),
            ));
        if ($block) {
            $this->getResponse()->setBody($block->toHtml());
        }
    }
}