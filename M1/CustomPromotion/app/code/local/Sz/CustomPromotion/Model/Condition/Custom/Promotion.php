<?php

class Sz_CustomPromotion_Model_Condition_Custom_Promotion extends Mage_Rule_Model_Condition_Abstract
{

    /**
     * @return Sz_CustomPromotion_Model_Condition_Tweet
     */
    public function loadAttributeOptions()
    {
        $attributes = array(
            'custompromotion' => Mage::helper('custompromotion')->__('Already Applied Rules')
        );
        $this->setAttributeOption($attributes);
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAttributeElement()
    {
        $element = parent::getAttributeElement();
        $element->setShowAsText(true);
        return $element;
    }

    /**
     * @return string
     */
    public function getInputType()
    {
        return 'string';
    }

    /**
     * @return string
     */
    public function getValueElementType()
    {
        return 'text';
    }


    /**
     * Validate Rule Condition
     * @param Varien_Object $object
     * @return bool
     */
    public function validate(Varien_Object $model)
    {
        $ruleIds = $model;
        if ($model->getQuote()) {
            $appliedCouponCode = $model->getQuote()->getCouponCode();
            if (!$appliedCouponCode) {
                return false;
            }
            if (!$this->_validateCouponCode($appliedCouponCode)) {
               return false;
            } else {
                $ruleInfo = Mage::getModel('salesrule/coupon')->load(
                    $appliedCouponCode, 'code'
                );
                try {
                    $ruleIds->setData('custompromotion', $ruleInfo->getRuleId());
                } catch (Exception $e) {
                    Mage::logException($e);
                }
            }
        }
        return parent::validate($ruleIds);
    }



    protected function _validateCouponCode($appliedCouponCode = null)
    {
        if ($appliedCouponCode) {
            if (strlen($appliedCouponCode)) {
                $addressHasCoupon = false;
                $addresses = $this->getAllAddresses();
                if (count($addresses)>0) {
                    foreach ($addresses as $address) {
                        if ($address->hasCouponCode()) {
                            $addressHasCoupon = true;
                        }
                    }
                    if (!$addressHasCoupon) {
                        $this->setCouponCode('');
                    }
                }
            }
            return $this;
        }
    }

    /**
     * Retrieve after element HTML
     *
     * @return string
     */
    public function getValueAfterElementHtml()
    {
        $image = Mage::getDesign()->getSkinUrl('images/rule_chooser_trigger.gif');
        $html = '<a href="javascript:void(0)" class="rule-chooser-trigger"><img src="'
            . $image
            . '" alt="" class="v-middle rule-chooser-trigger" title="'
            . Mage::helper('core')->quoteEscape(Mage::helper('rule')->__('Open Chooser'))
            . '" /></a>';
        return $html;
    }


    /**
     * Enable chooser selection button
     *
     * @return bool
     * @SuppressWarnings(PHPMD.BooleanGetMethodName)
     */
    public function getExplicitApply()
    {
        return true;
    }


    /**
     * Chooser URL getter
     *
     * @return string
     */
    public function getValueElementChooserUrl()
    {
        $url = 'adminhtml/custompromotion/chooser';
        if ($this->getJsFormObject()) {
            $url .= '/form/'.$this->getJsFormObject();
        }
        return Mage::helper('adminhtml')->getUrl($url);
    }

    /**
     * Default operator input by type map getter
     *
     * @return array
     */
    public function getDefaultOperatorInputByType()
    {
        return array('string' => array('()', '!()'));
    }
}