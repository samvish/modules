<?php
/**
 * Implements observer functionality for module.
 *
 *
 * @category   Sz
 * @package    Sz_CustomPromotion
 * @subpackage Config
 * @version    Release: 0.1.0
 * @author     UpBott Team <sam@upbott.com>
 */

class Sz_CustomPromotion_Model_Observer
{


    /**
     * @param $observer
     * @return bool
     */
    public function addConditionToSalesRule($observer)
    {
        try {
            $currentStore = Mage::app()->getStore();
            if (!$this->_getHelper()->isEnable($currentStore->getId())) {
                return false;
            }
            $additional = $observer->getAdditional();
            $conditions = (array)$additional->getConditions();

            $conditions = array_merge_recursive($conditions, array(
                array(
                    'label' => $this->_getHelper()->__('Already Applied Rules'),
                    'value' => 'custompromotion/condition_custom_promotion'
                ),
            ));
            $additional->setConditions($conditions);
            $observer->setAdditional($additional);
        } catch (Exception $e) {
            Mage::logException($e);
        }
        return $observer;
    }

    protected function _getHelper()
    {
        return Mage::helper('custompromotion');
    }
}