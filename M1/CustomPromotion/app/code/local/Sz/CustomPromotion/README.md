**Custom Promotion Module**

*Description*
- Module can be used to create shopping cart promotions at Item specific
---

## How to Enable Custom Promotion Rule Condition?
1. Go To Admin -> System -> Configuration -> Custom Promotion -> General -> Enable
2. Set Enable flag to Yes and see in shopping cart price rule that new condition is being added

## How to Create a Promotion Rule at Item Specific?
1. Go to Admin Panel - Promotion - Shopping Cart Price Rules
2. Create Promotion Rule with Coupon type (specific or auto-generated) 
3. Do not provide any actions or conditions for this rule (unless and until want to provide discount on top of the its child promotion rules).
4. Set Priority of this rule as zero (or less than the priority of its child rule) so that it will apply on cart before its child rules. 
5. Once rule get creteated, create child rule with priority greater than parent rule.
6. In child rule, go to Condition tab of its Admin detail page and select condition as Applied Parent Rules (based on need you can use other condition along with this).
7. once we select this Applied parent rules condition, you can see we will be able to select all coupon type rules.
8. Select one or more rules whose coupon code you want to use for applying this child rule on cart page.
9. Now Go to Action Tab of this child rule and provided required action (like discount type, amount etc)
10. also provide the specific condition in Action tab (depend on need for example: SKU : Test) on which you want to apply discount over cart


---

## How the discount going to apply?

1. Parent rule coupon will be applied first over cart
2. once coupon code of parent rule validated and applied, its child rule will be applied over cart depending on their priority and settings.



